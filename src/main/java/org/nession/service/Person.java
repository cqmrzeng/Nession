
package org.nession.service;

import java.io.Serializable;

/**
 * @author wangyj9
 * @version 1.0
 * @since 2013年12月25日 上午10:50:01
 */
public class Person implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9018732275371337252L;
	private String name;
	private int age;

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public int getAge()
	{
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(int age)
	{
		this.age = age;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Person [name=" + name + ", age=" + age + "]";
	}
}
